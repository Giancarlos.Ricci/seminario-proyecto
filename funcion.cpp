#include <iostream>
#include <math.h>

int main(){

    float a,b,c;
    float discriminante,x1,x2,Vx,Vy,vertice;

    std::cout<<"Bienvenido a la calculadora de caracteristicas de una funcion cuadratica."<<std::endl;

    std::cout<<"Ingrese el valor de a:"<<std::endl;
    std::cin>>a;
    if (a==0){
        std::cout<<"El valor ingresado para a no corresponde a una ecuacion cuadratica"<<std::endl;
        return 1;
    }
    std::cout<<"Ingrese el valor de b:"<<std::endl;
    std::cin>>b;
    std::cout<<"Ingrese el valor de c:"<<std::endl;
    std::cin>>c;

    discriminante = (b*b)-(4*a*c);
    x1 = (-b+sqrt(discriminante))/(2*a);
    x2 = (-b-sqrt(discriminante))/(2*a);
    Vx = (-b)/(2*a);
    Vy = a*Vx*Vx + b*Vx + c;

    std::cout<<"La interseccion con el eje Y es:"<< c <<std::endl;
    std::cout<<"El discriminante es:"<< discriminante <<std::endl;
    std::cout<<"La raiz positiva es:"<< x1 <<std::endl;
    std::cout<<"La raiz negativa es:"<< x2 <<std::endl;
    std::cout<<"El vertice es: ("<< Vx <<","<< Vy <<")"<<std::endl;


    return 0;
}